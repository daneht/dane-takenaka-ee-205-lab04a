///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Dane Takenaka <daneht@hawaii.edu>
// @date   2/2/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>



int main() {

   struct tm ref_time;
   time_t ref;
   ref_time.tm_year = 1999 -1900;
   ref_time.tm_mon = 2;
   ref_time.tm_mday = 10;
   ref_time.tm_hour = 4;
   ref_time.tm_min = 40;
   ref_time.tm_sec = 26;
   ref_time.tm_isdst = 0;

   // This is the reference time Mar 10 1999
   long ref_total;   // in seconds
   ref_total = 63014820026;


   long your_year;
   long your_month;
   long your_monthi; // the amount of days passed in the respective month
   long your_day;
   long your_hour;
   long your_min;
   long your_sec;
   long your_total; // in sec

   printf("Enter a year (eg. 2020): \n");
      scanf("%ld", &your_year);
   printf("Enter a month (press 1 for Jan): \n");
      scanf("%ld", &your_month);
   printf("Enter a day (eg. 27): \n");
      scanf("%ld", &your_day);
   printf("Enter a hour (eg. 22): \n");
      scanf("%ld", &your_hour);
   printf("Enter a minute (eg. 38): \n");
      scanf("%ld", &your_min);
   printf("Enter a second (eg. 49): \n");
      scanf("%ld", &your_sec);




   if(your_month == 1){
      your_monthi = 0;};

   if(your_month == 2){
      your_monthi = 31;};

   if(your_month == 3){
      your_monthi = 59;};

   if(your_month == 4){
      your_monthi = 90;};

   if(your_month == 5){
      your_monthi = 120;};

   if(your_month == 6){
      your_monthi = 151;};

   if(your_month == 7){
      your_monthi = 181;};

   if(your_month == 8){
      your_monthi = 212;};

   if(your_month == 9){
      your_monthi = 243;};

   if(your_month == 10){
      your_monthi = 273;};

   if(your_month == 11){
      your_monthi = 304;};

   if(your_month == 12){
      your_monthi = 334;};



   your_total = ((your_year - 1) * 31536000) + ((your_monthi) * 86400) + ((your_day - 1) * 86400) + (your_hour * 60 * 60) + (your_min * 60) + (your_sec);



   //printf("ref is   : %ld  %ld  %ld  %ld  %ld  %ld \n", ref_year, ref_month, ref_day, ref_hour, ref_min, ref_sec);
   //printf("You chose: %ld  %ld  %ld  %ld  %ld  %ld \n", your_year, your_month, your_day, your_hour, your_min, your_sec); //checks to see if data is being collected

   //printf("your_monthi = %ld \n", your_monthi); // checks to see if your_monthi converts to the correct days

   //printf("your_total = %ld \n", your_total);
   
   //printf("ref_total = %ld \n", ref_total);

   ref =  mktime(&ref_time);
   printf("Reference time:  ");
   printf(ctime(&ref));
   printf("\n");

   for(your_total <= ref_total; your_total <= ref_total; your_total = your_total + 1){
      long sec_diff = ref_total - your_total;
      long year     = (sec_diff) / 31536000;
      long day      = ((sec_diff) - (31536000 * year)) / 86400;
      long hour     = ((sec_diff) - (31536000 * year) - (86400 * day)) / 3600;
      long min      = ((sec_diff) - (31536000 * year) - (86400 * day) - (3600 * hour)) / 60;
      long sec      = ((sec_diff) - (31536000 * year) - (86400 * day) - (3600 * hour) - (60 * min));

      printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %ld \n", year, day, hour, min, sec);
      
      if(your_total == ref_total){
         printf("A legend is born\n");
            };

      sleep(1);

       };


   for(your_total > ref_total; your_total > ref_total; your_total = your_total + 1){
      long sec_diff = your_total - ref_total;
      long year     = (sec_diff) / 31536000;
      long day      = ((sec_diff) - (31536000 * year)) / 86400;
      long hour     = ((sec_diff) - (31536000 * year) - (86400 * day)) / 3600;
      long min      = ((sec_diff) - (31536000 * year) - (86400 * day) - (3600 * hour)) / 60;
      long sec      = ((sec_diff) - (31536000 * year) - (86400 * day) - (3600 * hour) - (60 * min));

      printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %ld \n", year, day, hour, min, sec);

      sleep(1);
      };
};

